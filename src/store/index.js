import Vue from "vue";
import Vuex from "vuex";
import { APIFetchers } from "../components/Question/QuestionAPI";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    difficulty: "",
    amount: "",
    category: "",
    categories: [],
    questions: [],
    userAnswers: [],
    score: 0,
    isFetching: false,
  },
  mutations: {
    setDifficulty: (state, payload) => {
      state.difficulty = payload;
    },
    setAmount: (state, payload) => {
      state.amount = payload;
    },
    setCategory: (state, payload) => {
      state.category = payload;
    },
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    addAnswer: (state, payload) => {
      state.userAnswers.push(payload);
    },
    scoreUp: (state) => {
      state.score = state.score + 10;
    },
  },
  getters: {
    getDifficulty: (state) => {
      return state.difficulty;
    },
    getAmount: (state) => {
      return state.amount;
    },
    getCategory: (state) => {
      return state.category;
    },
    getCategories: (state) => {
      return state.categories;
    },
    getQuestions: (state) => {
      return state.questions;
    },
    getAnswers: (state) => {
      return state.userAnswers;
    },
    getScore: (state) => {
      return state.score;
    },
    getIsFetching: (state) => {
      return state.isFetching;
    },
  },
  actions: {
    async retrieveCategories({ state, commit }) {
      state.isFetching = true;
      try {
        const categories = await APIFetchers.fetchCategories();
        commit("setCategories", categories);
      } catch (error) {
        commit("setCategories", []);
      }
      state.isFetching = false;
    },
    async retrieveQuestions({ state, commit }) {
      state.isFetching = true;
      state.score = 0;
      state.userAnswers = [];

      try {
        const questions = await APIFetchers.fetchQuestions(
          state.amount,
          state.category,
          state.difficulty
        );
        if (questions) {
          console.log(questions);
          commit("setQuestions", questions);
        }
      } catch (error) {
        commit("setQuestions", []);
      }
      state.isFetching = false;
    },
  },
});
