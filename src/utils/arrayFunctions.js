//Takes an array and randomizes the order.
export const shuffleArray = (array) => {
  let i = array.length;
  if (i > 1) {
    while (--i) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }
  return array;
};
