import { shuffleArray } from "../../utils/arrayFunctions";

export const APIFetchers = {
  //Fetches all the categories
  async fetchCategories() {
    const response = await fetch("https://opentdb.com/api_category.php");
    if (response.status !== 200) {
      throw new Error("Could not fetch categories!");
    }
    const json = await response.json();
    const categories = json.trivia_categories;
    return categories.map((category) => ({ ...category }));
  },

  //Adds both id and an array with all the answers. The answers are shuffled when the question is of type multiple.
  async fetchQuestions(amount, category, difficulty) {
    const response = await fetch(
      `https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}`
    );
    if (response.status !== 200) {
      throw new Error("Could not fetch questions!");
    }
    const json = await response.json();
    const questions = json.results;
    return questions.map((question, i) => ({
      id: i,
      type: question.type,
      question: question.question,
      correct_answer: question.correct_answer,
      answers:
        question.type == "boolean"
          ? ["True", "False"]
          : shuffleArray([
              question.correct_answer,
              question.incorrect_answers[0],
              question.incorrect_answers[1],
              question.incorrect_answers[2],
            ]),
    }));
  },
};
