import Vue from "vue";
import VueRouter from "vue-router";
import StartPage from "./routes/StartPage.vue";
import QuestionPage from "./routes/QuestionPage.vue";
import ResultPage from "./routes/ResultPage.vue";

Vue.use(VueRouter); //adding the Router to the Vue Object
const routes = [
  {
    path: "/",
    component: StartPage,
  },
  {
    path: "/question",
    component: QuestionPage,
  },
  {
    path: "/results",
    component: ResultPage,
  },
];
export default new VueRouter({ routes });
